'use strict';

const _ = require('lodash'),
	striptags = require('striptags');

const allowedColumn = (allowedColumns, column) => {
	_.isString(column) && allowedColumns.length && allowedColumns.indexOf(column) >= 0;
};

const getSafeString = (str, allowSpaces, allowedTags) => {
	if (!allowSpaces) str = str.trim();
	return striptags(str, allowedTags);
};

const stripString = (params, attr, allowSpaces, allowedTags) => {
	params[attr] = getSafeString(params[attr], allowSpaces, allowedTags);
	return;
};

const stripArray = (params, attr, allowSpaces, allowedTags, allowedColumns) => {
	params[attr] = params[attr].map(param => {
		if (_.isObject(param)) return stripObject(param, allowSpaces, allowedTags, allowedColumns);
		if (_.isString(param)) return getSafeString(param, allowSpaces, allowedTags);
		return param;
	});
};

/**
 * Sanitize the strings received in body
 *
 * @param {object} params The body of the request
 * @param {boolean} allowSpaces [optional] if true, does not performs trim() to string
 * @param {array} allowedTags [optional] if received tags will not be removed
 * @param {array} allowedColumns [optional] if receive object key will not be analized
 */
const stripObject = (params, allowSpaces, allowedTags, allowedColumns) => {
	if (_.isObject(params)) {
		Object.keys(params).forEach(attr => {
			if (!allowedColumn(allowedColumns, params[attr])) {
				if (_.isString(params[attr])) return stripString(params, attr, allowSpaces, allowedTags);
				if (_.isObject(params[attr])) return stripObject(params[attr], allowSpaces, allowedTags, allowedColumns);
				if (Array.isArray(params[attr])) return stripArray(params, attr, allowSpaces, allowedTags, allowedColumns);
			}
		});
	}
	return;
};

exports.loadStriptagsMiddleware = (req, config, next) => {
	stripObject(req.body, config.allowSpaces || false, config.allowedTags || [], config.allowedColumns || []);
	return next();
};
