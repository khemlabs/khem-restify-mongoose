'use strict';

const express = require('express'),
	router = express.Router(),
	log = require('khem-log'),
	restify = require('express-restify-mongoose'),
	kauth = require('khem-auth'),
	{ requireMiddlewares, loadMiddleware, loadLibMiddleware } = require('./helpers'),
	{ loadStriptagsMiddleware } = require('./striptags');

const krestify = {
	restify,

	loadStriptags(config, modelName) {
		const preCreate = (req, res, next) => loadStriptagsMiddleware(req, config, next);
		const preUpdate = (req, res, next) => loadStriptagsMiddleware(req, config, next);
		// Load preCreate authentication method
		loadLibMiddleware(config, preCreate, 'preCreate');
		// Load preUpdate authentication method
		loadLibMiddleware(config, preUpdate, 'preUpdate');
	},

	loadAuthentication(config, middlewares, modelName, roles) {
		const methods = {
			preCreate: (req, res, next) => kauth.authenticateRoles(req, res, next, modelName, 'create'),
			preUpdate: (req, res, next) => kauth.authenticateRoles(req, res, next, modelName, 'update'),
			preDelete: [(req, res, next) => kauth.authenticateRoles(req, res, next, modelName, 'delete')],
			preRead: (req, res, next) => {
				return req.params.id
					? kauth.authenticateRoles(req, res, next, modelName, 'read')
					: kauth.authenticateRoles(req, res, next, modelName, 'list');
			}
		};
		if (roles === true) {
			// Load preCreate authentication method
			loadMiddleware(config, methods, middlewares, 'preCreate');
			// Load preUpdate authentication method
			loadMiddleware(config, methods, middlewares, 'preUpdate');
			// Load preDelete authentication method
			loadMiddleware(config, methods, middlewares, 'preDelete');
			// Load preRead authentication method
			loadMiddleware(config, methods, middlewares, 'preRead');
		} else {
			// Load preMiddleware authentication method (authentication for all methods with roles)
			loadMiddleware(config, methods, middlewares, 'preMiddleware');
		}
		config = { ...config, ...middlewares };
	},

	importModel(name, model) {
		if (!model.config) model.config = {};

		// Require middlewares
		let middlewares = requireMiddlewares(name);

		this.loadStriptags(model.config, middlewares, name);

		if (model.authenticate) {
			this.loadAuthentication(model.config, middlewares, name, model.authenticate);
		} else {
			model.config = { ...model.config, ...middlewares };
		}

		return restify.serve(router, model.schema, model.config);
	},

	router(models) {
		const keys = Object.keys(models);
		keys.forEach(model => this.importModel(model, models[model]));
		return router;
	}
};

module.exports = krestify;
