# README

Khemlabs - Kickstarter - restify-mongoose

### What is this repository for?

This lib is intended for developers that are using the khemlabs kickstarter framework, it
is a wrapper of the express-restify-mongoose lib

### Requirements

> Khemlabs kickstarter server

### Creating a middleware

Example at server/middlewares/restify-mongoose/Example.middleware.js

### Who do I talk to?

- dnangelus repo owner and admin
- developer elgambet and khemlabs
