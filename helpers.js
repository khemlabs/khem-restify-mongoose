'use strict';

const fs = require('fs'),
	_ = require('lodash'),
	log = require('khem-log');

const dirname = __dirname.replace('node_modules/khem-restify-mongoose', 'server/');
const path_middlewares = `${dirname}middlewares/restify-mongoose`;

const onError = (err, req, res, next) => next(err);

exports.requireMiddlewares = middlewareName => {
	log.info(`Loading (restify-mongoose) middlewares for: ${middlewareName}`);
	const path = `${path_middlewares}/${middlewareName}.middleware.js`;
	let middlewares = {};
	if (fs.existsSync(path)) {
		try {
			middlewares = require(path);
			if (!middlewares.onError) middlewares.onError = onError;
		} catch (err) {
			log.error(err, 'node_modules/khem-restify-mongoose/index.js', 'requireMiddlewares');
		}
	} else {
		middlewares.onError = onError;
	}
	return middlewares;
};

exports.loadLibMiddleware = (config, method, type) => {
	if (!config[type]) config[type] = [];
	config[type].push(method);
};

exports.loadMiddleware = (config, methods, middlewares, type) => {
	if (_.isFunction(methods[type])) methods[type] = [methods[type]];
	if (_.isFunction(middlewares[type])) {
		methods[type].push(middlewares[type].bind({}));
	} else if (_.isArray(middlewares[type])) {
		methods[type] = methods[type].concat(middlewares[type]);
	}
	delete middlewares[type];
	if (!config[type]) config[type] = [];
	config[type] = config[type].concat(methods[type]);
};
